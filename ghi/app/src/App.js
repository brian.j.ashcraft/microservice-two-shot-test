import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import { useState, useEffect } from 'react';

function App() {
  const [hats, setHats] = useState([]);

  useEffect(() => {
    loadHats();
  }, []);

  async function loadHats() {
    try {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error('Error occurred while loading hats:', error);
    }
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route
            path="/hats"
            element={<HatsList hats={hats} setHats={setHats} />}
          />
          <Route path="/hats/new" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;



// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import MainPage from './MainPage';
// import Nav from './Nav';
// import HatsList from './HatsList';
// import HatForm from './HatsForm';

// function App(hats) {
//   if (hats === undefined) {
//     return null;
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//           <Route path="/" element={<MainPage />} />
//           <Route path='hats'>
//             <Route index element={<HatsList hats={hats}/>} />
//             <Route path='new' element={<HatForm />} />
//           </Route>
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }

// export default App;
