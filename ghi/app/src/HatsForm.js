import React, {useEffect, useState} from "react";

function HatsForm() {
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    //handlers
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
        }
    }


    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.picture = picture;
        data.color = color;
        data.location = location;
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);

            if(response.ok) {
                const newLocation = await response.json();
                console.log(newLocation)
                setFabric('');
                setStyleName('');
                setColor('');
                setPicture('');
                setLocation('')
            }
    }



    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStyleNameChange} placeholder="StyleName" required type="text" name="style_name" id="style_name" className="form-control" value={styleName} />
                  <label htmlFor="starts">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                  <label htmlFor="ends">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureChange}  required type="url" name="picture" id="picture" className="form-control" value={picture} />
                  <label htmlFor="picture">Picture URL</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
                    <option value=''>Choose a location</option>
                    {locations.map((location) =>(
                        <option key={location.href} value={location.id}>{location.closet_name}</option>
                    ))}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default HatsForm
