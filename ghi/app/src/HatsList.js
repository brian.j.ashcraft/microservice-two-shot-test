import React from 'react';

function HatsList({ hats, setHats }) {
    const handleDelete = async (hatId) => {
        try {
            const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                // Remove the deleted hat from the list
                const updatedHats = hats.filter((hat) => hat.id !== hatId);
                setHats(updatedHats);
            } else {
                console.error('Failed to delete hat:', response.status);
            }
        } catch (error) {
            console.error('Error occurred while deleting hat:', error);
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {hats.map((hat) => (
                    <tr key={hat.id}>
                        <td>{hat.fabric}</td>
                        <td>{hat.style_name}</td>
                        <td>
                            <button
                                className="btn btn-danger"
                                onClick={() => handleDelete(hat.id)}
                            >
                                Delete
                            </button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default HatsList;







// import React from 'react';

// export default function HatsList(props) {
//     console.log("PROPS:", props);


//     return (
//         <table className="table table-striped">
//             <thead>
//                 <tr>
//                     <th>Fabric</th>
//                     <th>Style name</th>
//                 </tr>
//             </thead>
//             <tbody>
//                 {props.hats.hats?.map(hat => (
//                     <tr key={hat.id}>
//                         <td>{hat.fabric}</td>
//                         <td>{hat.style_name}</td>
//                     </tr>
//                 ))}
//             </tbody>
//         </table>
//     );
// }
