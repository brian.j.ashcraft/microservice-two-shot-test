from django.db import models

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=25)
    location_href = models.CharField(max_length=25)

class Hat(models.Model):
    fabric =  models.CharField(max_length=25)
    style_name = models.CharField(max_length=25)
    color = models.CharField(max_length=25)
    picture = models.URLField()

    location = models.ForeignKey(
        LocationVO,
        related_name="hat_location",
        on_delete=models.CASCADE,
    )