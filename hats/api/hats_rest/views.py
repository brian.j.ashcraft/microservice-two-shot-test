from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Create your views here.

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
    "fabric", 
    "style_name", 
    "color",  
    "picture",
    "location",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        location_id = content.pop("location")  # Remove the location ID from the content
        location = LocationVO.objects.get(id=location_id) # Retrieve the LocationVO object
        hats = Hat.objects.create(location=location, **content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
