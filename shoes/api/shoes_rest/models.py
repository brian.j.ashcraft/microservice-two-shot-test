from django.db import models

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=50)
    bin_href = models.CharField(max_length=50)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe_bin",
        on_delete=models.CASCADE,
    )
